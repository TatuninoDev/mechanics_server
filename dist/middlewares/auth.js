"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.validTokenAdmin = exports.validToken = void 0;
const token_1 = __importDefault(require("../classes/token"));
const validToken = (req, res, next) => {
    const userToken = req.get('x-token') || '';
    token_1.default.validateToken(userToken).then((decoded) => {
        req.user = decoded.user;
        next();
    }).catch(err => {
        res.json({
            ok: false,
            message: 'Token invalido!'
        });
    });
};
exports.validToken = validToken;
const validTokenAdmin = (req, res, next) => {
    const userToken = req.get('x-token') || '';
    token_1.default.validateToken(userToken).then((decoded) => {
        req.user = decoded.user;
        if (req.user.role === 'Admin') {
            next();
        }
        else {
            res.json({
                ok: false,
                message: 'No eres admin'
            });
        }
    }).catch(err => {
        res.json({
            ok: false,
            message: 'El token no es valido!'
        });
    });
};
exports.validTokenAdmin = validTokenAdmin;
