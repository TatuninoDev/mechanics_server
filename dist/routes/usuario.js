"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt_nodejs_1 = __importStar(require("bcrypt-nodejs"));
const express_1 = require("express");
const token_1 = __importDefault(require("../classes/token"));
const auth_1 = require("../middlewares/auth");
const user_model_1 = require("../models/user.model");
const userRoutes = express_1.Router();
const salt = bcrypt_nodejs_1.genSaltSync(10);
//login
userRoutes.post('/login', (req, res) => {
    const body = req.body;
    user_model_1.User.findOne({ user: body.user }, (err, userDB) => {
        if (err)
            throw err;
        if (!userDB) {
            return res.json({
                ok: false,
                message: 'Usuario / contraseña no son correctos'
            });
        }
        if (userDB.comparePassword(body.password)) {
            const tokenUser = token_1.default.getToken({
                _id: userDB._id,
                nombre: userDB.nombre,
                user: userDB.user,
                avatar: userDB.avatar,
                role: userDB.role
            });
            res.json({
                ok: true,
                token: tokenUser
            });
        }
        else {
            return res.json({
                ok: false,
                message: 'Usuario / contraseña no son correctos'
            });
        }
    });
});
// Crear usuario
userRoutes.post('/create', (req, res) => {
    const user = {
        nombre: req.body.nombre,
        user: req.body.user,
        email: req.body.email,
        password: bcrypt_nodejs_1.default.hashSync(req.body.password, salt),
        role: req.body.role,
        avatar: req.body.avatar
    };
    user_model_1.User.create(user).then(userDB => {
        const tokenUser = token_1.default.getToken({
            _id: userDB._id,
            nombre: userDB.nombre,
            user: userDB.user,
            avatar: userDB.avatar,
            role: userDB.role
        });
        res.json({
            ok: true,
            token: tokenUser
        });
    }).catch(err => {
        res.json({
            ok: false,
            err
        });
    });
});
// Actualizar usuario
userRoutes.post('/update', auth_1.validToken, (req, res) => {
    const user = {
        nombre: req.body.nombre || req.user.nombre,
        avatar: req.body.avatar || req.user.avatar,
        email: req.body.email || req.user.avatar,
    };
    user_model_1.User.findByIdAndUpdate(req.user._id, user, { new: true }, (err, userDB) => {
        if (err)
            throw err;
        if (!userDB) {
            return res.json({
                ok: false,
                message: 'No existe un usuario con es ID'
            });
        }
        const tokenUser = token_1.default.getToken({
            _id: userDB._id,
            nombre: userDB.nombre,
            user: userDB.user,
            avatar: userDB.avatar,
            role: userDB.role
        });
        res.json({
            ok: true,
            user: tokenUser
        });
    });
});
// obtener user
userRoutes.get('/', [auth_1.validToken], (req, res) => {
    const usuario = req.user;
    res.json({
        ok: true,
        usuario
    });
});
exports.default = userRoutes;
