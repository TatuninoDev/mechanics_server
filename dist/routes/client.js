"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const client_model_1 = require("./../models/client.model");
const express_1 = require("express");
const auth_1 = require("../middlewares/auth");
const bcrypt_nodejs_1 = __importStar(require("bcrypt-nodejs"));
const token_1 = __importDefault(require("../classes/token"));
const clientRoutes = express_1.Router();
const salt = bcrypt_nodejs_1.genSaltSync(10);
clientRoutes.get('/', [auth_1.validToken], (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let page = Number(req.query.page) || 1;
    let skip = page - 1;
    skip = skip * 10;
    if (req.user.role === 'Admin' || req.user.role === 'admin') {
        const data = yield client_model_1.Client.find().skip(skip)
            .limit(10)
            .exec();
        res.json({
            ok: true,
            page,
            data
        });
    }
    else {
        const data = yield client_model_1.Client.find({ user: req.user.user }).exec();
        res.json({
            ok: true,
            page,
            data
        });
    }
}));
clientRoutes.post('/create', [auth_1.validToken], (req, res) => {
    const client = {
        name: req.body.name,
        phone: req.body.phone,
        email: req.body.email,
        marcaVehiculo: req.body.marcaVehiculo,
        modelo: req.body.modelo,
        year: req.body.year,
        matricula: req.body.matricula,
        user: req.body.user,
        password: bcrypt_nodejs_1.default.hashSync(req.body.password, salt),
    };
    client_model_1.Client.create(client).then(clientDB => {
        const tokenUser = token_1.default.getToken({
            _id: clientDB._id,
            name: clientDB.name,
            user: clientDB.user,
            phone: clientDB.phone,
            email: clientDB.email,
            marcaVehiculo: clientDB.marcaVehiculo,
            modelo: clientDB.modelo,
            year: clientDB.year,
            matricula: clientDB.matricula
        });
        const addTokenDB = {
            _id: clientDB._id || req.body.name,
            name: clientDB.name || req.body.phone,
            user: clientDB.user || req.body.email,
            phone: clientDB.phone || req.body.marcaVehiculo,
            email: clientDB.email || req.body.modelo,
            marcaVehiculo: clientDB.marcaVehiculo || req.body.year,
            modelo: clientDB.modelo || req.body.matricula,
            year: clientDB.year || req.body.user,
            matricula: clientDB.matricula || bcrypt_nodejs_1.default.hashSync(req.body.password, salt),
            token: tokenUser
        };
        client_model_1.Client.findByIdAndUpdate(clientDB._id, addTokenDB, { new: true }, (err, clientdB) => {
            if (err)
                throw err;
            if (!clientdB) {
                return res.json({
                    ok: false,
                    message: 'No existe un usuario con es ID'
                });
            }
        });
        res.json({
            ok: true,
            token: tokenUser,
            message: 'Cliente creado satisfactoriamente!!'
        });
    }).catch(err => {
        res.json({
            ok: false,
            err
        });
    });
    // update client
});
exports.default = clientRoutes;
