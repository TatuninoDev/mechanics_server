"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const maintenance_model_1 = require("./../models/maintenance.model");
const express_1 = require("express");
const auth_1 = require("../middlewares/auth");
const maintenanceRoutes = express_1.Router();
// lista de mantenimientos
maintenanceRoutes.get('/', [auth_1.validToken], (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let page = Number(req.query.page) || 1;
    let skip = page - 1;
    skip = skip * 10;
    const today = new Date();
    let result = `${today.getDate()}/${today.getMonth()}/${today.getFullYear()}`;
    if (req.user.role === 'Admin' || req.user.role === 'admin') {
        const data = yield maintenance_model_1.Maintenance.find({ 'nextMaintenance': result })
            .skip(skip)
            .limit(10)
            .populate('user', '-password')
            .exec();
        if (data.length > 0) {
            res.json({
                ok: true,
                page,
                data
            });
        }
        else {
            res.json({
                ok: false,
                page,
                message: 'No hay mantenimientos disponibles'
            });
        }
    }
    else {
        const data = yield maintenance_model_1.Maintenance.find({ user: req.user, 'nextMaintenance': result }).exec();
        if (data.length > 0) {
            res.json({
                ok: true,
                data
            });
        }
        else {
            res.json({
                ok: false,
                message: 'No hay mantenimientos disponibles'
            });
        }
    }
}));
//crear mantenimiento
maintenanceRoutes.post('/', [auth_1.validToken], (req, res) => {
    const body = req.body;
    body.user = req.user._id;
    maintenance_model_1.Maintenance.create(body).then((dataDB) => __awaiter(void 0, void 0, void 0, function* () {
        yield dataDB.populate('user', '-password').execPopulate();
        res.json({
            ok: true,
            data: dataDB
        });
    })).catch(err => {
        res.json({
            ok: false,
            err
        });
    });
});
// Actualizar mantenimiento
maintenanceRoutes.post('/update', [auth_1.validTokenAdmin], (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const user = req.body.user;
    const dataUser = yield maintenance_model_1.Maintenance.find({ user }).populate('user', '-password').exec();
    const nextMaintenance = dataUser[0].nextMaintenance;
    const splitDate = nextMaintenance.toString().split('/');
    const formatDateTime = `${splitDate[2]}-${splitDate[1]}-${splitDate[0]} 12:00:00`;
    const convertDateTime = new Date(formatDateTime);
    const month = new Date(convertDateTime.setMonth(convertDateTime.getMonth() + 7));
    let resultPlusMonth = `${convertDateTime.getDate()}/${month.getMonth()}/${convertDateTime.getFullYear()}`;
    const previousMaintenances = dataUser[0].previousMaintenance;
    previousMaintenances.push(nextMaintenance);
    const updateMaintenance = {
        description: req.body.description || req.user.description,
        nextMaintenance: resultPlusMonth,
        previousMaintenance: previousMaintenances
    };
    maintenance_model_1.Maintenance.findByIdAndUpdate(dataUser[0]._id, updateMaintenance, { new: true }, (err, updateDB) => {
        if (err)
            throw err;
        if (!updateDB) {
            return res.json({
                ok: false,
                menssage: 'No existe un usuario con ese ID'
            });
        }
        res.json({
            ok: true,
            message: 'Se actualizo correctamente'
        });
    });
}));
exports.default = maintenanceRoutes;
