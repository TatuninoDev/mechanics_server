"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Client = void 0;
const mongoose_1 = require("mongoose");
const clientSchema = new mongoose_1.Schema({
    name: {
        type: String,
        required: [true, 'El nombre es requerido']
    },
    phone: {
        type: String,
        required: [true, 'El telefono es requerido']
    },
    email: {
        type: String,
        unique: true,
        required: [true, 'El correo es requerido']
    },
    user: {
        type: String,
        unique: true,
        required: [true, 'El usuario es requerido']
    },
    password: {
        type: String,
        required: [true, 'la clave es requerido']
    },
    marcaVehiculo: {
        type: String,
        requered: [true, 'La marca del vehiculo es requerida']
    },
    modelo: {
        type: String,
        requered: [true, 'El modelo del vehiculo es requerido']
    },
    year: {
        type: String,
        requered: [true, 'El año es requerido']
    },
    matricula: {
        type: String,
        requered: [true, 'La matricula es requerida']
    },
    token: {
        type: String
    }
});
exports.Client = mongoose_1.model('Client', clientSchema);
