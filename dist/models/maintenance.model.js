"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Maintenance = void 0;
const mongoose_1 = require("mongoose");
const maintenanceSchema = new mongoose_1.Schema({
    date: {
        type: String
    },
    description: {
        type: String,
        required: [true, 'Debe de haber una descripcion']
    },
    previousMaintenance: [{
            type: String
        }],
    user: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Client',
        required: [true, 'Debe haber una referencia a un usuario']
    },
    nextMaintenance: {
        type: String
    }
});
maintenanceSchema.pre('save', function (next) {
    const date = new Date();
    this.date = `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;
    let maintenanceDate;
    maintenanceDate = new Date();
    maintenanceDate = new Date(maintenanceDate.setMonth(maintenanceDate.getMonth() + 7));
    this.nextMaintenance = `${maintenanceDate.getDate()}/${maintenanceDate.getMonth()}/${maintenanceDate.getFullYear()}`;
    next();
});
exports.Maintenance = mongoose_1.model('Maintenance', maintenanceSchema);
