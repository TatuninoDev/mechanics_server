"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Maintenance = void 0;
const mongoose_1 = require("mongoose");
const maintenanceSchema = new mongoose_1.Schema({
    date: {
        type: Date
    },
    description: {
        type: String,
        required: [true, 'Debe de haber una descripcion']
    },
    previousMaintenance: [{
            type: String
        }],
    email: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Client',
        required: [true, 'Debe haber una referencia a un correo']
    },
    maintenance: {
        type: Boolean
    }
});
maintenanceSchema.pre('save', function (next) {
    this.date = new Date();
    next();
});
exports.Maintenance = mongoose_1.model('Maintenance', maintenanceSchema);
