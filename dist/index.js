"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const server_1 = __importDefault(require("./classes/server"));
const mongoose_1 = __importDefault(require("mongoose"));
const body_parser_1 = __importDefault(require("body-parser"));
const usuario_1 = __importDefault(require("./routes/usuario"));
const maintenance_1 = __importDefault(require("./routes/maintenance"));
const cors_1 = __importDefault(require("cors"));
const client_1 = __importDefault(require("./routes/client"));
const server = new server_1.default();
server.app.use(body_parser_1.default.urlencoded({ extended: true }));
server.app.use(body_parser_1.default.json());
server.app.use(cors_1.default({ origin: true, credentials: true }));
// rutas 
server.app.use('/user', usuario_1.default);
server.app.use('/maintenance', maintenance_1.default);
server.app.use('/client', client_1.default);
// conexion a mongoDB
mongoose_1.default.connect('mongodb://admin:admin123@192.168.0.55:27017/mechanics?retryWrites=true&w=majority', { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true }, (err) => {
    if (err)
        throw err;
    console.log('Base de datos Online');
});
server.start(() => {
    console.log(`Server corriendo en el puerto: ${server.port}`);
});
