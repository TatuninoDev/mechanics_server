import { Client } from './../models/client.model';
import { Router, Response } from "express";
import { validToken, validTokenAdmin } from "../middlewares/auth";
import bcrypt, { genSalt, genSaltSync } from "bcrypt-nodejs";
import Token from "../classes/token";

const clientRoutes = Router();
const salt = genSaltSync(10);

clientRoutes.get('/', [ validToken ], async ( req: any, res: Response) => {

    let page = Number( req.query.page ) || 1;
    let skip = page - 1;
    skip = skip * 10;

    if(req.user.role === 'Admin' || req.user.role === 'admin'){

        const data =  await Client.find().skip( skip )
                                        .limit( 10 )
                                        .exec();
        
        res.json({
            ok: true,
            page,
            data
        });
    }else {

        const data =  await Client.find({user: req.user.user}).exec();
        
        res.json({
            ok: true,
            page,
            data
        });
    }

});

clientRoutes.post('/create', [validToken], (req: any, res: Response) => {
   const client = {
    name: req.body.name,
    phone: req.body.phone,
    email: req.body.email,
    marcaVehiculo: req.body.marcaVehiculo,
    modelo: req.body.modelo,
    year: req.body.year,
    matricula: req.body.matricula,
    user: req.body.user,
    password: bcrypt.hashSync( req.body.password, salt ),
   };

   Client.create( client ).then( clientDB => {
    const tokenUser = Token.getToken({
        _id: clientDB._id,
        name: clientDB.name,
        user: clientDB.user,
        phone: clientDB.phone,
        email: clientDB.email,
        marcaVehiculo: clientDB.marcaVehiculo,
        modelo: clientDB.modelo,
        year: clientDB.year,
        matricula: clientDB.matricula
    });

    const addTokenDB = {
        _id: clientDB._id || req.body.name,
        name: clientDB.name || req.body.phone,
        user: clientDB.user || req.body.email,
        phone: clientDB.phone || req.body.marcaVehiculo,
        email: clientDB.email || req.body.modelo,
        marcaVehiculo: clientDB.marcaVehiculo || req.body.year,
        modelo: clientDB.modelo || req.body.matricula,
        year: clientDB.year || req.body.user,
        matricula: clientDB.matricula || bcrypt.hashSync( req.body.password, salt ),
        token: tokenUser
    }

    Client.findByIdAndUpdate(clientDB._id, addTokenDB, { new: true }, (err, clientdB) => {
        if(err) throw err;
        if(!clientdB){
            return res.json({
            ok: false,
            message: 'No existe un usuario con es ID'
            });
        }
    })

    res.json({
        ok: true,
        token: tokenUser,
        message: 'Cliente creado satisfactoriamente!!'
    })
   }).catch( err => {
       res.json({
           ok: false,
           err
       })
   })

   // update client

});

export default clientRoutes;