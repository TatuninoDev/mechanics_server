import { Maintenance } from './../models/maintenance.model';
import { Router, Response } from "express";
import { validToken, validTokenAdmin } from "../middlewares/auth";

const maintenanceRoutes = Router();

// lista de mantenimientos
maintenanceRoutes.get('/', [ validToken ], async ( req: any, res: Response) => {

    let page = Number( req.query.page ) || 1;
    let skip = page - 1;
    skip = skip * 10;
    const today = new Date();
    let result = `${today.getDate()}/${today.getMonth()}/${today.getFullYear()}`;

    if(req.user.role === 'Admin' || req.user.role === 'admin'){

        const data =  await Maintenance.find({'nextMaintenance': result})
                                       .skip( skip )
                                       .limit( 10 )
                                       .populate('user', '-password')
                                       .exec();
                       
        if (data.length > 0) {
            res.json({
                ok: true,
                page,
                data
            });
    
        }else {
            res.json({
                ok: false,
                page,
                message: 'No hay mantenimientos disponibles'
            });
        }
    }else {
        const data =  await Maintenance.find({user: req.user, 'nextMaintenance': result}).exec();
        
        if(data.length > 0) {
            res.json({
                ok: true,
                data
            })

        }else {
            res.json({
                ok: false,
                message: 'No hay mantenimientos disponibles'
            })
            
        }
        
    }


});


//crear mantenimiento

maintenanceRoutes.post('/', [validToken], (req: any, res: Response) => {
    const body = req.body;
    body.user = req.user._id;

    Maintenance.create( body ).then( async dataDB => {
        await dataDB.populate('user', '-password').execPopulate();

        res.json({
            ok: true,
            data: dataDB
        })
    }).catch( err => {
        res.json({
            ok: false,
            err
        })
    } );
});

// Actualizar mantenimiento
maintenanceRoutes.post('/update', [validTokenAdmin], async (req: any, res: Response)  => {
    
    const user = req.body.user;

    const dataUser = await Maintenance.find({user}).populate('user', '-password').exec();

    const nextMaintenance = dataUser[0].nextMaintenance;

    const splitDate = nextMaintenance.toString().split('/');

    const formatDateTime = `${splitDate[2]}-${splitDate[1]}-${splitDate[0]} 12:00:00`;

    const convertDateTime = new Date(formatDateTime);

    const month = new Date(convertDateTime.setMonth( convertDateTime.getMonth() + 7 ));

    let resultPlusMonth = `${convertDateTime.getDate()}/${month.getMonth()}/${convertDateTime.getFullYear()}`;

    const previousMaintenances = dataUser[0].previousMaintenance;
    previousMaintenances.push(nextMaintenance);

    const updateMaintenance = {
        description : req.body.description || req.user.description,
        nextMaintenance: resultPlusMonth,
        previousMaintenance: previousMaintenances
    }
    
    
    Maintenance.findByIdAndUpdate( dataUser[0]._id, updateMaintenance, {new: true}, (err,updateDB) => {
        if ( err ) throw err;

        if ( !updateDB ) {
            return res.json({
                ok: false,
                menssage: 'No existe un usuario con ese ID'
            });
        }


        res.json({
            ok: true,
            message: 'Se actualizo correctamente'
        });
    } )
} )

export default maintenanceRoutes;