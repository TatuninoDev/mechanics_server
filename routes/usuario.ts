import bcrypt, { genSalt, genSaltSync } from "bcrypt-nodejs";
import { Router, Request, Response } from "express";
import Token from "../classes/token";
import { validToken } from "../middlewares/auth";
import { User } from "../models/user.model";

const userRoutes = Router();
const salt = genSaltSync(10);


//login
userRoutes.post('/login', (req: Request, res: Response) => {
    const body = req.body;

  User.findOne( { user: body.user }, ( err: any, userDB: any ) => {
    if( err ) throw err;
    if( !userDB ) {
      return res.json({
        ok: false,
        message: 'Usuario / contraseña no son correctos'
      });
    }

    if( userDB.comparePassword( body.password ) ) {

      const tokenUser = Token.getToken({
        _id: userDB._id,
        nombre: userDB.nombre,
        user: userDB.user,
        avatar: userDB.avatar,
        role: userDB.role
      });
      res.json({
        ok: true,
        token: tokenUser
      });
    } else {
      return res.json({
        ok: false,
        message: 'Usuario / contraseña no son correctos'
      });
    }

  });
});
// Crear usuario
userRoutes.post('/create', ( req: Request , res: Response ) => {

    const user = {
      nombre: req.body.nombre,
      user: req.body.user,
      email: req.body.email,
      password: bcrypt.hashSync( req.body.password, salt ),
      role: req.body.role,
      avatar: req.body.avatar
    };
  
  
  
    User.create( user ).then( userDB => {
  
      const tokenUser = Token.getToken({
        _id: userDB._id,
        nombre: userDB.nombre,
        user: userDB.user,
        avatar: userDB.avatar,
        role: userDB.role
      });
      
      res.json({
        ok: true,
        token: tokenUser
      })
  
    }).catch( err => {
      res.json({
        ok: false,
        err
      })
    });
  
  });
  
  // Actualizar usuario
  
  userRoutes.post('/update', validToken,( req: any , res: Response ) => {
  
    const user = {
      nombre: req.body.nombre || req.user.nombre,
      avatar: req.body.avatar || req.user.avatar,
      email: req.body.email || req.user.avatar,
    }
  
    User.findByIdAndUpdate( req.user._id, user, { new: true }, (err, userDB) =>{
      if(err) throw err;
      if(!userDB){
        return res.json({
          ok: false,
          message: 'No existe un usuario con es ID'
        });
      }
      const tokenUser = Token.getToken({
        _id: userDB._id,
        nombre: userDB.nombre,
        user: userDB.user,
        avatar: userDB.avatar,
        role: userDB.role
      });
      
      res.json({
        ok: true,
        user: tokenUser
      });
  
    });
  
  
  });
  
  // obtener user
  userRoutes.get('/', [validToken], (req:any, res:Response) => {
      
    const usuario = req.user;
    res.json({
        ok: true,
        usuario
    })
  })
  
  
  export default userRoutes;