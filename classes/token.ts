import jwt from 'jsonwebtoken';

export default class Token {
    private static seed: string = 'TatuninoDeveloperIsTheBestDeveloper';
    private static expiration: string = '10y';

    constructor() {}

    static getToken ( payload: any ): string {
        return jwt.sign({
            user: payload
        }, this.seed, {expiresIn: this.expiration});
    }

    static validateToken ( userToken: string ) {
        return new Promise( (resolve, reject) => {
            jwt.verify( userToken, this.seed, ( err, decoded ) => {
                if ( err ) {
                    reject();
                } else {
                    resolve( decoded );
                }
            })
        });
    }

}