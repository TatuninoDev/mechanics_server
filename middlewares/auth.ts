import { Response, Request, NextFunction } from 'express';
import { decode } from 'jsonwebtoken';
import Token from '../classes/token';

export const validToken = ( req: any, res: Response, next: NextFunction ) => {
    const userToken = req.get('x-token') || '';
    Token.validateToken( userToken ).then((decoded: any) => {
        req.user = decoded.user;
        next();
    }).catch( err => {
        res.json({
            ok: false,
            message: 'Token invalido!'
        });
    });
}

export const validTokenAdmin = ( req: any, res: Response, next: NextFunction ) => {
 
    const userToken = req.get( 'x-token' ) || '';
   
    Token.validateToken( userToken ).then( (decoded: any) => {

     req.user = decoded.user;
   
     if ( req.user.role === 'Admin' ) {
      next();
     } else {
      res.json({
       ok: false,
       message: 'No eres admin'
      });
     }
   
    }).catch( err => {
     res.json({
      ok: false,
      message: 'El token no es valido!'
     });
    });
   
   }