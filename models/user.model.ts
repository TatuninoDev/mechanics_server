import { Schema, model, Document } from 'mongoose';
import bcrypt from 'bcrypt-nodejs';

const userSchema = new Schema({
    name: {
        type: String,
        required: [true, 'El nombre es necesario']
    },
    avatar: {
        type: String,
        default: 'av-1.png'
    },
    user: {
        type: String,
        unique: true,
        required: [true, 'El usuario es necesario']
    },
    email: {
        type: String,
        unique: true,
        required: [true, 'El correo es necesario']
    },
    password: {
        type: String,
        required: [true, 'La contraseña es necesaria']
    },
    role: {
        type: String,
        required: [true, 'El rol es necesario']
    }
});

interface IUser extends Document {
    nombre: string;
    user: string,
    password: string;
    email: string;
    avatar: string;
    role:string;
    comparePassword(password: string) : boolean;
}

userSchema.method('comparePassword', function( password: string = '' ): boolean {
    if ( bcrypt.compareSync(password, this.password) ) {
        return true;
    } else {
        return false;
    }
});

export const User = model<IUser>('User', userSchema);