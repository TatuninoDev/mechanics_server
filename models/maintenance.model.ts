import { Schema, model, Document } from 'mongoose';

const maintenanceSchema = new Schema({
    date: {
        type: String
    },
    description: {
        type: String,
        required: [true, 'Debe de haber una descripcion']
    },
    previousMaintenance: [{
        type: String
    }],
    user: {
        type: Schema.Types.ObjectId,
        ref: 'Client',
        required: [true, 'Debe haber una referencia a un usuario']
    },
    nextMaintenance: {
        type: String
    }
});

export interface IMaintenance extends Document {
    description: String;
    previousMaintenance: String[];
    nextMaintenance: string;
    date: string;
}

maintenanceSchema.pre<IMaintenance>('save', function( next ) {
    const date = new Date();
    this.date = `${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}`;
    let maintenanceDate: Date;
    maintenanceDate = new Date();
    maintenanceDate = new Date(maintenanceDate.setMonth( maintenanceDate.getMonth() + 7 ));
    this.nextMaintenance = `${maintenanceDate.getDate()}/${maintenanceDate.getMonth()}/${maintenanceDate.getFullYear()}`;
    next();
});

export const Maintenance = model<IMaintenance>('Maintenance', maintenanceSchema);