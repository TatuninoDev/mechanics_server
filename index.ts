import Server from './classes/server';
import mongoose  from 'mongoose';
import bodyParser from 'body-parser';
import userRoutes from "./routes/usuario";
import maintenanceRoutes from "./routes/maintenance";
import cors from 'cors';
import clientRoutes from './routes/client';

const server = new Server();

server.app.use(bodyParser.urlencoded({extended: true}));
server.app.use(bodyParser.json());
server.app.use(cors({origin: true, credentials: true}));


// rutas 
server.app.use('/user', userRoutes);
server.app.use('/maintenance', maintenanceRoutes);
server.app.use('/client', clientRoutes);

// conexion a mongoDB
mongoose.connect('mongodb://admin:admin123@192.168.0.55:27017/mechanics?retryWrites=true&w=majority',{ useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true}, ( err ) => {
    if ( err ) throw err;
    console.log('Base de datos Online');
});

server.start( () => {
    console.log(`Server corriendo en el puerto: ${server.port}`);
});
